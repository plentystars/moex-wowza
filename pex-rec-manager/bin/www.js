const shared = require('../shared')(true, false, false);

initServer(shared.config.app.port);

function initServer(port) {
    const ws = require('http');
    const server = ws.createServer(require('../worker')(port));

    server.listen(port);
    // Event listener for HTTP(s) server "error" event.
    server.on('error', function (error) {
        if (error.syscall !== 'listen')
            throw error;

        let bind = typeof shared.config.app.port === 'string' ? 'Pipe ' + shared.config.app.port : 'Port ' + shared.config.app.port;

        // handle specific listen errors with friendly messages
        switch (error.code) {
            case 'EACCES':
                shared.utils.logger.log('error', 'PID#' + process.pid + ': ' + bind + ' requires elevated privileges');
                server.close(function () {
                    process.exit(1);
                });
                break;
            case 'EADDRINUSE':
                shared.utils.logger.log('error', 'PID#' + process.pid + ': ' + bind + ' is already in use');
                server.close(function () {
                    process.exit(1);
                });
                break;
            default:
                throw error;
        }
    });
    // Event listener for HTTP server "listening" event.
    server.on('listening', function () {
        let addr = server.address(),
            bind = typeof addr === 'string' ? 'pipe ' + addr : 'port ' + addr.port;
        shared.utils.logger.log('info', 'PID#' + process.pid + ': Express' + ' listening on ' + bind);
    });

    process.on('uncaughtException', function (err) {
        shared.utils.logger.log('error', err);
        server.close(function () {
            process.exit(1);
        });
    });

    return server;
}
