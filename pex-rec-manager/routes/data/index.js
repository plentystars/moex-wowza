module.exports = function (express) {
    const router = express.Router(),
        fs = require('fs-extra'),
        path = require('path');

    router.all('*', global.utils.uploader.any(), function (req, res, next) {
        let urlParts = req.path.split('/').slice(1),
            noAuth = ['/env/debug', '/env/user'];

        if (urlParts.length < 1)
            return res.status(400).send(global.utils.httpError(400).message);

        let pathView = [path.resolve(global.config.paths.root), 'routes', 'data'],
            controller = '';
        urlParts.forEach(function (item, index) {
            if (item !== '' && index < (urlParts.length)) {
                pathView.push(item);
            }
        });

        controller = path.normalize(pathView.join('/')) + '.js';
        if (!fs.existsSync(controller)) {
            pathView.push('index');
            controller = path.normalize(pathView.join('/')) + '.js';
        }

        if (!fs.existsSync(controller))
            return res.status(404).send(global.utils.httpError(404).message);

        let dataSource = require(controller),
            method = req.method.toLowerCase();

        if (!dataSource[method])
            return res.status(400).send(global.utils.httpError(400).message);

        if (noAuth.includes(req.path))
            return dataSource[method](req, res, next);
        else if (!req.isAuthenticated())
            return res.status(401).send('Error 401. Unauthorized');
        else
            return dataSource[method](req, res, next);
    });

    return router;
};
