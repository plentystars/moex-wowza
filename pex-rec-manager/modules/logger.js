
function Logger(options, service) {
    const winston = require('winston'),
        expressWinston = require('express-winston'),
        path = require('path');

    require('winston-daily-rotate-file');

    let levels = {
        levels: {
            auth: 0,
            dev: 1,
            error: 2,
            warn: 3,
            info: 4,
            http: 5,
            verbose: 6,
            debug: 7,
            silly: 8
        },
        colors: {
            auth: 'red',
            dev: 'red',
            error: 'red',
            warn: 'yellow',
            info: 'green',
            http: 'green',
            verbose: 'cyan',
            debug: 'blue',
            silly: 'magenta'
        }
    };

    winston.addColors(levels.colors);

    this._logger = winston.createLogger({
        level: 'silly',
        levels: levels.levels,
        format: winston.format.combine(
            winston.format.timestamp(),
            winston.format.json()
        ),
        transports: [
            new winston.transports.DailyRotateFile({
                level: 'info',
                handleExceptions: true,
                dirname: options.paths.logs,
                filename: 'full-%DATE%.log',
                datePattern: 'YYYY-MM-DD',
                maxSize: '10m',
                maxFiles: '30d'
            }),
            new winston.transports.DailyRotateFile({
                level: 'error',
                handleExceptions: true,
                dirname: options.paths.logs,
                filename: 'error-%DATE%.log',
                datePattern: 'YYYY-MM-DD',
                maxSize: '10m',
                maxFiles: '30d'
            }),
            new winston.transports.DailyRotateFile({
                level: 'dev',
                handleExceptions: true,
                dirname: options.paths.logs,
                filename: 'dev-%DATE%.log',
                datePattern: 'YYYY-MM-DD',
                maxSize: '10m',
                maxFiles: '30d'
            }),
            new winston.transports.File({
                filename: path.normalize(path.join(options.paths.logs, 'auth.log')),
                level: 'auth',
                format: winston.format.combine(
                    winston.format.timestamp(),
                    winston.format.simple()
                )
            })
        ],
        exitOnError: false,
        defaultMeta: {service: service}
    });

    if (options.app.debug)
        this._logger.add(
            new winston.transports.Console({
                level: 'silly',
                handleExceptions: true,
                format: winston.format.combine(
                    winston.format.colorize({
                        level: true
                    }),
                    winston.format.timestamp(),
                    winston.format.simple(),
                    winston.format.printf(function (info) {
                        if (info.meta)
                            return `[${info.timestamp}] ${info.level}: ${info.message}\n` + JSON.stringify(info.meta, null, '\t')
                                .replace(/\\n /g, '\n\t')
                                .replace(/\\nE/g, '\n\t E');
                        return `[${info.timestamp}] ${info.level}: ${info.message}`;
                    })
                )
            })
        );

    this.express = expressWinston.logger({
        winstonInstance: this._logger,
        expressFormat: true,
        meta: true
    });

    this.error = expressWinston.errorLogger({
        winstonInstance: this._logger
    });

    this.log = function (level, message, meta = undefined, cb = undefined) {
        function reformat(obj, isMeta) {
            if (typeof obj === 'object' && !isMeta) {
                if (Object.getOwnPropertyNames(obj).length)
                    return (obj.message + ' \n[' + obj.stack)
                        .replace(/\\n /g, '\n\t')
                        .replace(/\\nE/g, '\n\t E') + ']';
                else
                    return '';
            } else  if (typeof obj === 'object' && isMeta) {
                if (Object.getOwnPropertyNames(obj).length)
                    return JSON.stringify(obj)
                        .replace(/\\n /g, '\n\t')
                        .replace(/\\nE/g, '\n\t E') + ']';
                else
                    return '';
            }
            else
                return (' [' + obj.trim() + ']');
        }

        if (meta && (typeof meta === 'object' || typeof meta === 'string')) {
            this._logger.log(level, '[' + service + '] ' + message + ' ' + reformat(meta, true));

            if (cb && typeof cb === 'function')
                return cb();
        } else {
            this._logger.log(level, (typeof message === 'object' ? '[' + service + '] ' + message + reformat(message, false) : '[' + service + '] ' + message));

            if (meta && typeof meta === 'function')
                return meta();
            else if (cb && typeof cb === 'function')
                return cb();
        }
    };
}

module.exports = Logger;
