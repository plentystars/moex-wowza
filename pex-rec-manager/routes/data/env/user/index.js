
exports.get = function (req, res, next) {
    if (!req.isAuthenticated())
        return res.status(200).json({});

    return res.status(200).json(Object.assign({session_id: req.sessionID}, req.user));
};
