module.exports = function (config) {
    const passport = require('passport'),
        LocalStrategy = require('passport-local').Strategy;

    passport.serializeUser(function (user, done) {
        done(null, user);
    });

    passport.deserializeUser(function (user, done) {
        done(null, user);
    });

    passport.use(
        'local-login',
        new LocalStrategy({
                usernameField: 'username',
                passwordField: 'password',
                passReqToCallback: true
            },
            function (req, username, password, cb) {
                let user = global.config.users.find(function (u) {
                    return u.username.toLowerCase() === username.toLowerCase();
                });

                if (!user)
                    return cb(null, null, 'Ошибка аворизации<br/>Пользователь не обнаружен');

                if (password !== user.password)
                    return cb(null, null, 'Ошибка аворизации<br/>Введен неправильный пароль');

                return cb(null, user, 'Вход выполнен успешно<br/>Вы будете передресованы...');
            }
        )
    );

    return passport;
};
