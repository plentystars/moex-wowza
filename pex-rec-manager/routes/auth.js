const path = require("path");
module.exports = function (express) {
    const router = express.Router(),
        path = require('path'),
        fs = require('fs-extra');

    router.get('/login', function (req, res, next) {
        if (req.isAuthenticated())
            return res.status(301).redirect('/');

        return res.status(200).render(path.join(global.config.paths.views, 'auth', 'login.pug'), {});
    });

    router.post('/login', function (req, res, next) {
        global.utils.passport.authenticate('local-login', function (err, user, info) {
            if (err)
                return global.utils.logger.log('error', err, function () {
                    return res.status(500).send(err.message);
                });

            req.login(user, function (err) {
                if (err)
                    return global.utils.logger.log('error', err, function () {
                        return res.status(500).send(err.message);
                    });

                return res.status(201).json({
                    message: info
                });
            });
        })(req, res, next);
    });



    router.get('/logout', function (req, res, next) {
        if (!req.isAuthenticated())
            return res.redirect('/auth/login');

        let sessionID = req.sessionID;
        req.logout();
        req.session.destroy(function (err) {
            if (err)
                global.utils.logger.log('error', err);

            delete global.sessions[sessionID];

            return res.redirect('/auth/login');
        });
    });

    router.all('*', function (req, res, next) {
        return next(global.utils.httpError(400));
    });

    return router;
};
