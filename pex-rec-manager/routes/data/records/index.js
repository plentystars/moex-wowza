const path = require("path");
exports.get = function (req, res, next) {
    const path = require('path'),
        fs = require('fs-extra'),
        url = require('url'),
        filesize = require('filesize'),
        send = require('send'),
        readChunk = require('read-chunk'),
        fileType = require('file-type'),
        zip = require('archiver')('zip');

    let paramPath = path.join(global.config.paths.drive, req.query['path'] || ''),
        paramName = (req.query['name'] || path.basename(paramPath));

    switch (req.query['action'] || 'dir') {
        case 'dl':
            if ((req.query['type'] || 'file') === 'dir') {
                res.setHeader('Content-disposition', 'attachment; filename="' + encodeURI(paramName + '.zip') + '"');
                res.setHeader('Content-type', 'application/zip');

                zip.pipe(res);
                zip.directory(paramPath, path.basename(paramPath)).finalize();
            } else
                return send(req, paramPath)
                    .on('headers', function (res) {
                        let fType = fileType(readChunk.sync(paramPath, 0, fileType.minimumBytes));
                        res.setHeader('Content-disposition', 'attachment; filename="' + encodeURI(paramName) + '"');
                        res.setHeader('Content-type', (fType ? fType.mime : 'text/plain'));
                    })
                    .pipe(res);
            break;
        case 'dir':
        default:
            readPath(path.resolve(paramPath, '..'), paramPath, function (err, result) {
                if (err)
                    return global.utils.logger.log('error', err, function () {
                        return res.status(500).send(err.message);
                    });

                return res.status(200).json(result);
            });
            break;
    }

    function readPath(parentPath, currentPath, cb) {
        fs.readdir(currentPath, function (err, files) {
            if (err)
                return cb(err);

            let arrDirs = [],
                arrFiles = [];

            if (currentPath !== global.config.paths.drive) {
                arrDirs.push({
                    id: -2,
                    path: '',
                    type: 'root',
                    name: '.',
                    size: '&#8249;Dir&#8250;',
                    uri: url.resolve('https://' + global.config.hostname, '/drive'),
                    modified: global.sessions[req.sessionID].moment(fs.statSync(global.config.paths.drive).mtime).tz(req.session.tz).format('DD.MM.YYYY HH:mm:ss'),
                    info: {system: true}
                });

                let pathNormalized = path.normalize(parentPath.replace(global.config.paths.drive, '') || '');
                arrDirs.push({
                    id: -1,
                    path: (pathNormalized === '.' ? '' : pathNormalized),
                    type: 'root',
                    name: '..',
                    size: '&#8249;Dir&#8250;',
                    uri: url.resolve('https://' + global.config.hostname, '/drive' + parentPath.replace(global.config.paths.drive, '')),
                    modified: global.sessions[req.sessionID].moment(fs.statSync(parentPath).mtime).tz(req.session.tz).format('DD.MM.YYYY HH:mm:ss'),
                    info: {system: true}
                });
            }

            files.forEach(function (fileName, index) {
                let fullPath = path.normalize(path.join(currentPath, fileName).replace(global.config.paths.drive, '')),
                    fileInfo = fs.statSync(path.join(currentPath, fileName));

                if (fileInfo.isDirectory())
                    arrDirs.push({
                        id: index,
                        path: fullPath,
                        type: 'dir',
                        name: fileName,
                        size: '&#8249;Dir&#8250;',
                        uri: url.resolve('https://' + global.config.hostname, '/drive' + fullPath),
                        modified: global.sessions[req.sessionID].moment(fileInfo.mtime).tz(req.session.tz).format('DD.MM.YYYY HH:mm:ss'),
                        info: {system: global.config.drive.sysfolders.includes(fileName)}
                    });
                else if (fileInfo.isFile())
                    arrFiles.push({
                        id: index,
                        path: fullPath,
                        type: 'file',
                        name: fileName,
                        ext: path.extname(fileName).split('.').join('').toLowerCase(),
                        size: filesize(fileInfo.size),
                        uri: url.resolve('https://' + global.config.hostname, '/drive' + fullPath),
                        modified: global.sessions[req.sessionID].moment(fileInfo.mtime).tz(req.session.tz).format('DD.MM.YYYY HH:mm:ss'),
                        info: {system: false}
                    });
            });

            return cb(null, arrDirs.concat(arrFiles));
        });
    }
};

exports.post = function (req, res, next) {
    const fs = require('fs-extra'),
        path = require('path');

    if (req.body['type'] === '')
        return res.status(500).end();

    switch (req.body['type']) {
        case 'dir':
            if ((req.body['name'] || '') === '')
                return res.status(500).end();

            fs.ensureDir(path.normalize(path.join(global.config.paths.drive, req.body['path'], req.body['name'])), function (err) {
                if (err)
                    return global.utils.logger.log('error', err, function () {
                        return res.status(500).send(err.message);
                    });
                return res.status(201).end();
            });
            break;
        case 'file':
            if (req.files.length === 0)
                return res.status(500).end();

            try {
                req.files.forEach(function (item) {
                    fs.moveSync(path.normalize(item.path), path.normalize(path.join(global.config.paths.drive, req.body['path'], item.originalname)), {overwrite: true});
                });
                return res.status(201).end();
            } catch (err) {
                return global.utils.logger.log('error', err, function () {
                    return res.status(500).send(err.message);
                });
            }
        default:
            return res.status(500).end();
    }
};

exports.put = function (req, res, next) {
    const fs = require('fs-extra'),
        path = require('path');

    if ((req.body['path'] || '') === '' || (req.body['name']) === '')
        return res.status(500).end();

    let fullPath = path.normalize(path.join(global.config.paths.drive, req.body['path']));

    fs.rename(fullPath, path.join(path.dirname(fullPath), req.body['name']), function (err) {
        if (err)
            return global.utils.logger.log('error', err, function () {
                return res.status(500).send(err.message);
            });
        return res.status(204).end();
    });
};

exports.delete = function (req, res, next) {
    const fs = require('fs-extra'),
        path = require('path');

    if ((req.body['path'] || '') === '')
        return res.status(500).end();

    fs.remove(path.normalize(path.join(global.config.paths.drive, req.body['path'])), function (err) {
        if (err)
            return global.utils.logger.log('error', err, function () {
                return res.status(500).send(err.message);
            });

        return res.status(204).end();
    });
};