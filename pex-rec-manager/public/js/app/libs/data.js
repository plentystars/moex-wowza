
const processAjax = function (params, cb) {
    params.async = params.async === undefined ? true : params.async;
    params.cache = params.cache === undefined ? true : params.cache;
    params.processData = params.processData === undefined ? true : params.processData;
    params.data = params.data === undefined ? {tz: jstz.determine().name()} : $.extend(true, params.data, {tz: jstz.determine().name()});
    params.dataType = params.dataType === undefined ? 'json' : params.dataType;
    params.contentType = params.contentType === undefined ? 'application/x-www-form-urlencoded; charset=UTF-8' : params.contentType;

    let result = false;
    $.ajax({
        type: params.type,
        url: params.url,
        data: params.data,
        dataType: params.dataType,
        async: params.async,
        cache: params.cache,
        contentType: params.contentType,
        processData: params.processData
    })
        .done(function (data) {
            result = ((data === '' || data === undefined) ? true : data);
            if (params.async && cb)
                return cb(result);
        })
        .fail(function (xhr) {
            if (params.dataType === 'html') {
                result = xhr.responseText;
                initApp.notification('error', (params.action !== undefined ? params.action + '<br/>' : '') +
                    'Error ' + xhr.status + '. ' + xhr.statusText, {});
            } else
                initApp.notification('error', (params.action !== undefined ? params.action + '<br/>' : '') +
                    'Error ' + xhr.status + '. ' + xhr.statusText + ((xhr.responseText === undefined || xhr.responseText === '') ? '' : '<br/>' + xhr.responseText), {});

            if (xhr.status === 401)
                setTimeout(function () {
                    window.location = '/auth/login';
                }, 2000);

            if (params.async && cb)
                return cb(result);
        });

    if (!params.async) {
        if (cb === undefined)
            return result;
        else
            return cb(result);
    }
};

const select2selected = function (selector) {
    let selected = [];
    $.each($(selector).select2('data'), function (index, option) {
        selected.push({id: option.id, text: option.text, new: (option.new !== undefined)});
    });
    return selected;
};

const datatable2selected = function (dt, options) {
    if (!Array.isArray(options))
        options = [options];

    if (options.length === 0)
        return [];

    let selectedData = [];
    dt.rows({selected: true}).every(function () {
        let rowData = this.data(),
            itemData = {};

        options.forEach(function (option) {
            itemData[option] = rowData[option];
        });

        selectedData.push(itemData);
    });

    return selectedData;
};

const querystring = function(obj, prefix) {
    var str = [],
        p;
    for (p in obj) {
        if (obj.hasOwnProperty(p)) {
            var k = prefix ? prefix + "[" + p + "]" : p,
                v = obj[p];
            str.push((v !== null && typeof v === "object") ?
                serialize(v, k) :
                encodeURIComponent(k) + "=" + encodeURIComponent(v));
        }
    }
    return str.join("&");
}