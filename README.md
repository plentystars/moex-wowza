# ВВОДНЫЕ УСЛОВИЯ: 

- [ ] имя сервера "recorder.pexip.moex.ru"
- [ ] wowza streaming engine установлен и настроен на автозапуск
- [ ] Открыты порты 
- TCP 1935 RTMP/RTMPE/RTMPT/RTSP-interleaved streaming and WOWZ™ streaming
- TCP 8086-8088 Administration
- TCP 80 Для управления сервером записи
- [ ] У сервера есть доступ к серверам лицензирования
- wowzalicense1.wowzamedia.com 
- wowzalicense2.wowzamedia.com 
- wowzalicense3.wowzamedia.com 
- wowzalicense4.wowzamedia.com


## Установка компонент

```
$ sudo bash
# cd /opt
# git clone https://gitlab.com/plentystars/moex-wowza.git .
# tar --strip-components 1 -xvf node-v16.13.0-linux-x64.tar.xz -C /usr
```

Проверяем

```
# node --version && npm --version
v16.13.0
8.1.0
```

```
# cd pex-rec-manager
# npm install
# cp pex-rec-manager.service /etc/systemd/system/pex-rec-manager.service
# systemctl daemon-reload
```

## Настройка прложения

```
# cp config/default.json.example config/default.json
```
В config/default.json
- [ ] hostname - вписываем ДНС-имя хоста - recorder.pexip.moex.ru
- [ ] wowza - вписываем ДНС-имя хоста с портом для консоли wowza streaming engine - http://recorder.pexip.moex.ru:8088
- [ ] вписываем пользователей/пароли в массив users, стандартного пользователя можно удалить или сменить ему пароль и передать данные заказчику

## Запуск прложения


```
# systemctl enable pex-rec-manager.service && systemctl start pex-rec-manager.service
Created symlink from /etc/systemd/system/multi-user.target.wants/pex-rec-manager.service to /etc/systemd/system/pex-rec-manager.service.
# systemctl status pex-rec-manager.service
● pex-rec-manager.service - Pexip Infinity and Wowza Streaming Engine records manager
   Loaded: loaded (/etc/systemd/system/pex-rec-manager.service; enabled; vendor preset: disabled)
   Active: active (running) since Tue 2021-11-23 15:54:26 MSK; 1min 19s ago
 Main PID: 1432 (node)
   CGroup: /system.slice/pex-rec-manager.service
           └─1432 /usr/bin/node bin/www.js

Nov 23 15:54:26 localhost.localdomain systemd[1]: Started Pexip Infinity and Wowza Streaming Engine records manager.
...
```

## Установка Wowza

Получить ключ и дистрибутив https://www.wowza.com/pricing/installer

```
# cd
# wget https://www.wowza.com/downloads/WowzaStreamingEngine-4-8-15+3/WowzaStreamingEngine-4.8.15+3-linux-x64-installer.run
# chmod 0750 WowzaStreamingEngine-4.8.15+3-linux-x64-installer.run
# ./WowzaStreamingEngine-4-8-15+3/WowzaStreamingEngine-4.8.15+3-linux-x64-installer.run
```

Следовать инструкциям установщика.

## Проверка Wowza

```
# curl http://localhost:1935/ServerVersion
<html><head><title>Wowza Streaming Engine 4 Perpetual Pro Edition 4.8.15+3 build20211022114044</title></head><body>Wowza Streaming Engine 4 Perpetual Pro Edition 4.8.15+3 build20211022114044</body></html>
```

## Настройка Wowza

- Идем в консоль wowza http://recorder.pexip.moex.ru:8088/enginemanager/ 
- переходим в меню Applications 
- проверяем приложение live 
- В SELECTED APPLICATION выбираем live
- на основном экране кнопка Edit. 
- Меняем:
- [ ] Playback Types - все
- [ ] Options - все
- [ ] Streaming File Directory - Use the following directory - /opt/_storage/
- [ ] Closed Caption Sources - None
- Save
- Переходим в Source Security, на основном экране кнопка Edit. Меняем:
- [ ]  RTMP Sources - Open (no authentication required)
- [ ]  RTSP Sources - Open (no authentication required)
- [ ]  Client Restrictions - No client restrictions
- [ ]  Duplicate Stream Names - Reject a second stream with the same name that's published to this application
- Save
- Переходим в Playback Security, на основном экране кнопка Edit. Меняем: 
- [ ]  Options - Require secure RTMP connection - снять
- [ ]  SecureToken - Do NOT use SecureToken
- [ ]  Client Restrictions - No client restrictions
- Save
- Сохраняем приложение и перезапускаем. (Restart Now сверху)

## Проверка приложения
- Идем http://recorder.pexip.moex.ru, логинимся, должны увидеть в списке записей запись sample.mp4 - пробуем скачать и/или нажать на "Воспроизвести"

## ДОНАСТРОЙКА PEXIP INFINITY

1. Идем на менеджмент-ноду.
2. Меню Services - Web App customization - Download current branding
3. Качаем и сохраняем branding_nextgen_custom.zip
4. В папке установки открываем recording.plugin.js и ищем строку "var recorder_uri = 'rtmp://recorder.pexip.moex.ru:1935/live/'" и меняем в ней имя сервера (recorder.pexip.moex.ru) на нужное
5. Упаковываем recording.plugin.js в архив по пути branding_nextgen_custom.zip/webapp2/plugins/recording/recording.plugin.js
6. Открываем прям в архиве файл по пути: branding_nextgen_custom.zip/webapp2/plugins/translator/translator.plugin.js
7. Ищем там набор строк вида: <option value="${this.vmr}_en">English</option> и в каждой строке меняем "${this.vmr}" на "${this.vmr.split('@')[0]}" чтоб строки преобрели такой вид: <option value="${this.vmr.split('@')[0]}_en">English</option>
8. Так меняем все строки по языкам. Сохраняем.
9. Идем на менеджмент-ноду
10. Меню Services - Web App customization - Upload Web App branding (applied system-wide) и загружаем branding_nextgen_custom.zip который мы поменяли ранее.


