
exports.get = function (req, res, next) {
    return res.status(200).send(global.config.app.timezone || 'Etc/UTC');
};
