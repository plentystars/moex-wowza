
$().ready(function () {
    let dtTable = undefined,
        clipboard = undefined,
        currentPath = '';

    const create = function (path, name) {
        processAjax({
            type: 'POST',
            url: '/data/records',
            data: {
                path: path,
                name: name,
                type: 'dir'
            },
            dataType: 'text'
        }, function (result) {
            if (result)
                return initApp.notification('success', 'Успешно сохранено!', {}, function () {
                    dtTable.ajax.reload(function () {
                        $('#modal-records-helper').modal('hide');
                        $('[data-toggle="tooltip"]').tooltip();
                    }, false);
                });
        });
    }, rename = function (path, name) {
        processAjax({
            type: 'PUT',
            url: '/data/records',
            data: {
                path: path,
                name: name
            },
            dataType: 'text'
        }, function (result) {
            if (result)
                return initApp.notification('success', 'Успешно сохранено!', {}, function () {
                    dtTable.ajax.reload(function () {
                        $('#modal-records-helper').modal('hide');
                        $('[data-toggle="tooltip"]').tooltip();
                    }, false);
                });
        });
    }, remove = function (path) {
        processAjax({
            type: 'DELETE',
            url: '/data/records',
            data: {
                path: path
            },
            dataType: 'text'
        }, function (result) {
            if (result)
                return initApp.notification('success', 'Успешно сохранено!', {}, function () {
                    dtTable.ajax.reload(function () {
                        $('[data-toggle="tooltip"]').tooltip();
                    }, false);
                });
        });
    };

    const pageFunction = function () {
        const columns = [{
            data: 'id',
            searchable: false,
            orderable: false,
            visible: false
        }, {
            data: 'path',
            searchable: false,
            orderable: false,
            visible: false
        }, {
            data: 'uri',
            searchable: false,
            orderable: false,
            visible: false
        }, {
            data: 'type',
            orderable: false,
            width: '5%',
            class: 'center',
            render: function (data, type, row, meta) {
                if (data === 'root')
                    return '<i class="fiv-ext fiv-size-md fiv-icon-folder-open"></i>';
                else if (data === 'dir')
                    return '<i class="fiv-ext fiv-size-md fiv-icon-folder"></i>';
                else
                    return '<i class="fiv-sqo fiv-size-md fiv-icon-' + row.ext + '"></i>';
            }
        }, {
            data: 'name',
            orderable: false,
            render: function (data, type, row, meta) {
                if (row.type === 'root')
                    return '[ ' + data + ' ]';
                else if (row.type === 'dir')
                    return '[ ' + data + ' ]';
                else
                    return data;
            }
        }, {
            data: 'size',
            orderable: false,
            width: '10%',
            class: 'text-right'
        }, {
            data: 'modified',
            orderable: false,
            width: '10%',
            class: 'center'
        }, {
            data: 'type',
            orderable: false,
            width: '1%',
            render: function (data, type, row, meta) {
                if (data === 'dir')
                    return '<div class="btn-group btn-group-sm">' +
                        '<button id="btn-records-rename-' + row.id + '" type="button" class="btn btn-outline-warning" data-records="rename" data-toggle="tooltip" data-placement="top" title="Переименовать"><i class="fal fa-lg fa-pen"></i></button>' +
                        '<button id="btn-records-delete-' + row.id + '" type="button" class="btn btn-outline-danger" data-records="delete" data-toggle="tooltip" data-placement="top" title="Удалить"><i class="fal fa-lg fa-trash"></i></button>' +
                        '<button id="btn-records-download-' + row.id + '" type="button" class="btn btn-outline-success" data-records="download" data-toggle="tooltip" data-placement="top" title="Скачать"><i class="fal fa-lg fa-download"></button>' +
                        '</div>';
                else if (data === 'file')
                    return '<div class="btn-group btn-group-sm">' +
                        '<button id="btn-records-rename-' + row.id + '" type="button" class="btn btn-outline-warning" data-records="rename" data-toggle="tooltip" data-placement="top" title="Переименовать"><i class="fal fa-lg fa-pen"></i></button>' +
                        '<button id="btn-records-delete-' + row.id + '" type="button" class="btn btn-outline-danger" data-records="delete" data-toggle="tooltip" data-placement="top" title="Удалить"><i class="fal fa-lg fa-trash"></i></button>' +
                        '<button id="btn-records-download-' + row.id + '" type="button" class="btn btn-outline-success" data-records="download" data-toggle="tooltip" data-placement="top" title="Скачать"><i class="fal fa-lg fa-download"></i></button>' +
                        ((row.ext || '').toLowerCase() === 'mp4' ? '<button id="btn-records-play-' + row.id + '" type="button" class="btn btn-outline-success" data-records="play" data-toggle="tooltip" data-placement="top" title="Воспроизвести"><i class="fal fa-lg fa-play"></i></button>' : '') +
                        '<button id="btn-records-link-' + row.id + '" type="button" class="btn btn-outline-info" data-clipboard-copy-link data-clipboard-action="copy" data-clipboard-text="' + row.uri + '" data-toggle="tooltip" data-placement="top" title="Скопировать ссылку"><i class="fal fa-lg fa-link"></i></button>' +
                        '</div>';
                else
                    return '';
            }
        }, {
            data: 'info',
            searchable: false,
            orderable: false,
            visible: false
        }];

        dtTable = datatable_nss('table#browser', '/data/records', function () {
                return {
                    action: 'dir',
                    path: currentPath
                };
            }, columns, [], [],
            {
                createdRow: function (row, data, dataIndex) {
                    if (data.type !== 'file')
                        $(row).addClass('fw-500');
                },
                select: false
            }, function () {
                $('[data-toggle="tooltip"]').tooltip();
            });

        clipboard = new Clipboard('[data-clipboard-copy-link]');
        clipboard.on('success', function (e) {
            return initApp.notification('info', 'Ссылка скопирована!');
        });

        $('table#browser tbody')
            .on('dblclick', 'tr', function (e) {
                e.preventDefault();

                let rowData = dtTable.row(this).data();
                if (rowData.type === 'file')
                    return ;

                currentPath = rowData.path;
                dtTable.ajax.reload(function () {
                    $('[data-toggle="tooltip"]').tooltip();
                }, true);
            })
            .on('click', 'button', function (e) {
                e.preventDefault();

                let data = dtTable.row($(this).parents('tr')).data();

                switch ($(this).data('records') || '') {
                    case 'play':
                        $('h5#modal-records-view-title').text(data.name);
                        $('video#modal-records-view-player').empty().append('<source type="video/mp4" src="/data/records/stream?path=' + data.path + '">');
                        $('#modal-records-view').modal('show');
                        break;
                    case 'rename':
                        $('h5#modal-records-helper-title').text(data.type === 'dir' ? 'Переименовать папку' : 'Переименовать файл');

                        $('input#modal-records-helper-action').val('rename');
                        $('input#modal-records-helper-path').val(data.path);
                        $('input#modal-records-helper-name').val(data.name);

                        $('#modal-records-helper').modal('show');
                        break;
                    case 'delete':
                        return remove(data.path);
                    case 'download':
                        return $.fileDownload('/data/records?action=dl&type=' + data.type + '&name=' + encodeURI(data.name) + '&path=' + encodeURI(data.path));
                    default:
                        break;
                }
            });
    };

    initApp.load_batch([
        {type: 'style', name: '/css/file-icon-vectors/file-icon-extra.css'},
        {type: 'style', name: '/css/file-icon-vectors/file-icon-square-o.css'},
        {type: 'style', name: '/css/datagrid/datatables/datatables.bundle.css'},
        {type: 'script', name: '/js/datagrid/datatables/datatables.bundle.js'},
        {type: 'script', name: '/js/miscellaneous/fileDownload/jquery.fileDownload.js'},
        {type: 'script', name: '/js/miscellaneous/clipboard/clipboard.js'}
    ], pageFunction);

    $('button#btn-records-upload-file').on('click', function (e) {
        e.preventDefault();

        $('#panel-records.panel .panel-content').append('<input id="records-upload-file" type="file" style="display: none;">');
        $('input#records-upload-file')
            .change(function (e) {
                let formData = new FormData();
                formData.append('type', 'file');
                formData.append('path', decodeURI(currentPath));
                formData.append('file', e.target.files[0], e.target.files[0].name);
                processAjax({
                    type: 'POST',
                    url: '/data/records',
                    data: formData,
                    async: false,
                    cache: false,
                    contentType: false,
                    processData: false,
                    dataType: 'text'
                }, function (result) {
                    if (result)
                        initApp.notification('success', 'Успешно сохранено!', {}, function () {
                            dtTable.ajax.reload(function () {
                                $('[data-toggle="tooltip"]').tooltip();
                            }, false);
                        });
                    $('input#records-upload-file').remove();
                });
            })
            .trigger('click');
    });

    $('button#btn-records-create-folder').on('click', function (e) {
        e.preventDefault();

        $('h5#modal-records-helper-title').text('Создать папку');

        $('input#modal-records-helper-action').val('create');
        $('input#modal-records-helper-path').val(currentPath);

        return $('#modal-records-helper').modal('show');
    });

    $('button#modal-records-helper-button-save').on('click', function (e) {
        e.preventDefault();
        switch ($('input#modal-records-helper-action').val() || '') {
            case 'create':
                return create($('input#modal-records-helper-path').val(), $('input#modal-records-helper-name').val());
            case 'rename':
                return rename($('input#modal-records-helper-path').val(), $('input#modal-records-helper-name').val());
            default:
                return;
        }
    });

    $('#modal-records-helper')
        .on('hidden.bs.modal', function (e) {
            $('input#modal-records-helper-action').val('');
            $('input#modal-records-helper-path').val('');
            $('input#modal-records-helper-name').val('');
        });

    $('#modal-records-view')
        .on('hide.bs.modal', function (e) {
            $('video#modal-records-view-player').trigger('pause').empty();
        });
});