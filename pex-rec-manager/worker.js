module.exports = function (port) {
    const express = require('express'),
        bodyParser = require('body-parser'),
        compression = require('compression');
    const path = require('path');

    // worker settings & utils
    Object.assign(global, require('./shared')(false, false, true, port));

    const app = express();

    app.set('trust proxy', (global.config.app.clustered ? ['127.0.0.1/8', '::1/128'] : false));
    app.set('etag', true);

    // view engine setup
    app.set('views', global.config.paths.views);
    app.set('view engine', 'pug');
    app.set('view cache', !global.config.app.debug);
    app.locals.pretty = global.config.app.debug;

    //logger
    app.use(global.utils.logger.express);

    // helmet protection
    const helmet = require('helmet');
    app.use(helmet.dnsPrefetchControl());
    app.use(helmet.frameguard());
    app.use(helmet.hidePoweredBy());
    app.use(helmet.hsts());
    app.use(helmet.ieNoOpen());
    app.use(helmet.noSniff());
    app.use(helmet.xssFilter());

    // compression
    app.use(compression({
        level: 6,
        filter: function (req, res) {
            if (req.headers['x-no-compression'])
                return false;
            return compression.filter(req, res);
        }
    }));

    app.use(require('nocache')());

    app.use(require('serve-favicon')(path.join(global.config.paths.public, 'img', 'favicon', 'favicon.ico'), {
        maxAge: global.config.app.debug ? 0 : 86400000
    }));

    app.use(require('cookie-parser')(global.config.app.secret));

    app.use(bodyParser.text({
        limit: global.config.limits.body.text,
        type: 'text/*'
    }));
    app.use(bodyParser.json({
        limit: global.config.limits.body.json
    }));
    app.use(bodyParser.urlencoded({
        limit: global.config.limits.body.urlencoded,
        extended: true
    }));

    app.use(global.utils.session);

    app.set('case sensitive routing', false);
    app.set('strict routing', false);

    // ROUTES WITHOUT AUTHORIZATION
    app.use(express.static(global.config.paths.public, {
        dotfiles: 'allow',
        redirect: true,
        etag: true,
        lastModified: true,
        maxAge: global.config.app.debug ? 0 : 86400000
    }));

    app.use('/drive', express.static(global.config.paths.drive, {
        dotfiles: 'allow',
        redirect: true,
        etag: true,
        lastModified: true,
        maxAge: global.config.app.debug ? 0 : 86400000
    }));
    // ROUTES WITHOUT AUTHORIZATION. END

    app.use(function (req, res, next) {
        if (req.query['tz'] && req.session)
            req.session.tz = req.query['tz'];
        else
            req.session.tz = 'Etc/Universal';

        return next();
    });

    app.use(global.utils.passport.initialize());
    app.use(global.utils.passport.session());

    app.use(function (req, res, next) {
        if (!req.user || !req.session)
            return next();

        req.user.last_activity = global.utils.moment.utc().format();

        if (!global.sessions[req.sessionID])
            global.sessions[req.sessionID] = {
                moment: require('moment-timezone')
            };

        if (!global.sessions[req.sessionID].moment)
            global.sessions[req.sessionID].moment = require('moment-timezone');

        if (global.sessions[req.sessionID].moment.locale() !== 'ru')
            global.sessions[req.sessionID].moment.locale('ru');

        return next();
    });

    // ROUTES WITH AUTHORIZATION
    app.use('/auth', require('./routes/auth')(express));
    app.use('/data', require('./routes/data')(express));
    app.use('/', require('./routes/index')(express));
    // ROUTES WITH AUTHORIZATION. END

    // ERRORS HANDLERS
    app.use(global.utils.logger.error);
    // catch 404 and forward to error handler
    app.use(function (req, res, next) {
        return next(global.utils.httpError(404));
    });
    // error handler
    app.use(function (err, req, res, next) {
        if (err.code === 'EPERM')
            return;

        return res.status(err.status || 500).render('error', {error: err, title: global.config.app.title, user: req.user});
    });
    // ERRORS HANDLERS. END

    return app;
};
