
const datatable_nss = function (selector, url, data = {}, columns, defs = [], order = [], options = {}, cb) {
    let optionsDef = {
        ajax: {
            type: 'GET',
            url: url,
            data: function (tData, tSettings) {
                if (!tData['source'])
                    tData['source'] = 'table';

                tData['length'] = (tData['length'] === -1 ? 'ALL' : tData['length']);

                if (typeof data === 'function')
                    $.extend(true, tData, data(tData));
                else
                    $.extend(true, tData, data);
                $.extend(true, tData, {tz: jstz.determine().name()})
            },
            dataSrc: '',
            error: function (xhr, error, thrown) {
                initApp.notification('error', 'Error ' + xhr.status + '. ' + xhr.statusText +
                    ((xhr.responseText === undefined || xhr.responseText === '') ? '' : '<br/>' + xhr.responseText), {});
            }
        },
        serverSide: false,
        processing: true,
        deferRender: true,
        searching: true,
        autoWidth: false,
        responsive: true,
        fixedHeader: true,
        pagingType: 'full_numbers',
        select: {
            style: 'single',
            items: 'row',
            blurable: false
        },
        lengthMenu: [
            [10, 15, 25, 50, -1],
            [10, 15, 25, 50, 'Все']
        ],
        rowId: 'id',
        pageLength: 25,
        columns: columns,
        columnDefs: defs,
        order: order,
        buttons: [
            {
                text: '<i class="fal fa-sync"></i>',
                titleAttr: 'Refresh table data',
                className: 'btn btn-outline-default btn-icon ',
                action: function (e, dt, node, config) {
                    dt.ajax.reload(function () {$('[data-toggle="tooltip"]').tooltip()}, true);
                }
            }
        ],
        dom: "<'row mb-3'<'col-lg-12 col-xl-6 d-flex align-items-center justify-content-start'fB><'col-lg-12 col-xl-6 d-flex align-items-center justify-content-end'l>>" +
            "<'row'<'col-sm-12'tr>>" +
            "<'row'<'col-lg-12 col-xl-6'i><'col-lg-12 col-xl-6'p>>",
        language: {
            "info": "Показаны с _START_ по _END_ из _TOTAL_ записей",
            "decimal": "",
            "infoEmpty": "Показаны с 0 по 0 из 0 записей",
            "emptyTable": "Данные отсутствуют",
            "infoFiltered": "(отобрано из _MAX_ записей)",
            "infoPostFix": "",
            "lengthMenu": "Показывать по: _MENU_ записей",
            "loadingRecords": "Загрузка...",
            "thousands": ",",
            "processing": "Обработка...",
            "zeroRecords": "Совпадающих записей не найдено",
            "search": "",
            "searchPlaceholder": "Поиск...",
            "paginate": {
                "first": "<i class=\"fal fa-2x fa-angle-double-left\"></i>",
                "last": "<i class=\"fal fa-2x fa-angle-double-right\"></i>",
                "previous": "<i class=\"fal fa-2x fa-angle-left\"></i>",
                "next": "<i class=\"fal fa-2x fa-angle-right\"></i>"
            },
            "aria": {
                "sortAscending": ": активируйте для сортировки столбца по возрастанию",
                "sortDescending": ": активируйте для сортировки столбца по убыванию"
            },
            "select": {
                "rows": "%d строк выбрано"
            },
            "buttons": {
                "refresh-tooltip": "Обновить данные таблицы"
            }
        },
        fnInitComplete: cb
    };

    if (options['buttons']) {
        options.buttons.map(function (button) {
            optionsDef.buttons.push(button);
        });
        delete options['buttons'];
    }

    $.extend(true, optionsDef, options);

    return $(selector).DataTable(optionsDef);
};

const uuid = function () {
    return ([1e7] + -1e3 + -4e3 + -8e3 + -1e11).replace(/[018]/g, function (match) {
        return (match ^ crypto.getRandomValues(new Uint8Array(1))[0] & 15 >> match / 4).toString(16);
    });
};
