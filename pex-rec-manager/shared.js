const path = require("path");
const crypto = require("crypto");

module.exports = function (init, daemon, worker, port) {
    // Global libraries. DO NOT TOUCH!!!
    global.bluebird = require('bluebird');
    global.bluebird.config({cancellation: true});
    // Global libraries. END

    const path = require('path'),
        fs = require('fs-extra');

    let shared = {};
    shared.cache = {};
    shared.config = require('config');
    shared.config.app.debug = (process.env.NODE_ENV || 'development') === 'development';
    shared.config.paths = {
        root: __dirname,
        logs: path.join(__dirname, 'logs'),
        temp: path.join(__dirname, 'temp'),
        uploads: path.join(__dirname, 'temp', 'uploads'),
        public: path.join(__dirname, 'public'),
        views: path.join(__dirname, 'views'),
        modules: path.join(__dirname, 'modules'),
        sessions: path.join(__dirname, 'sessions'),
        drive: (shared.config.drive.storage.type === 'relative' ? path.resolve(path.join(__dirname, shared.config.drive.storage.path)) : path.resolve(shared.config.drive.storage.path))
    };

    shared.utils = {
        logger: new (require('./modules/logger'))(shared.config, (init ? 'master' : (daemon ? 'daemon' : 'worker ' + port))),
        guid: function (length) {
            return require('nanoid').customAlphabet('0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz', (length ? length : 32))();
        }
    };

    if (init) {
        shared.config.app.port = normalizePort(process.env.PORT || shared.config.app.port);

        Object.keys(shared.config.paths).forEach(function (key) {
            fs.ensureDirSync(shared.config.paths[key]);
        });
    }

    if (worker) {
        shared.utils.httpError = require('http-errors');

        shared.utils.moment = require('moment-timezone');
        shared.utils.moment.locale('ru');

        // session store
        const session = require('express-session'),
            sessionFileStore = require('session-file-store')(session);

        // sessions
        shared.utils.session = session({
            secret: shared.config.app.secret,
            name: shared.config.session.cookie,
            resave: true,
            rolling: true,
            saveUninitialized: true,
            unset: 'destroy',
            store: new sessionFileStore({
                path : shared.config.paths.sessions
            }),
            cookie: {
                path: '/',
                httpOnly: true,
                secure: false,
                maxAge: shared.config.app.debug ? (86400000 * 30) : shared.config.session.ttl
            },
            genid: function (req) {
                return global.utils.guid();
            }
        });
    }

    if (worker) {
        shared.sessions = [];
        shared.utils.passport = require('./modules/passport')(shared.config);

        const multer = require('multer'),
            crypto = require('crypto');
        shared.utils.uploader = multer({
            limits: {
                fileSize: shared.config.limits.file
            },
            storage: multer.diskStorage({
                destination: function (req, file, next) {
                    return next(null, shared.config.paths.uploads);
                },
                filename: function (req, file, next) {
                    if (path.extname(file.originalname).trim() !== '')
                        return  next(null, crypto.createHash('md5').update(new Date().getTime().toString() + file.originalname).digest('hex').toLowerCase() + path.extname(file.originalname));

                    let fileExt = require('file-type')(file.buffer);
                    return next(null, crypto.createHash('md5').update(new Date().getTime().toString() + file.originalname).digest('hex').toLowerCase() + (fileExt ? '.' + fileExt.ext.toLowerCase() : ''));
                }
            })
        });
    }

    return shared;

    // Normalize a port into a number, string, or false.
    function normalizePort(val) {
        let port = parseInt(val, 10);
        // named pipe
        if (isNaN(port))
            return val;
        // port number
        if (port >= 0)
            return port;
        return false;
    }
};
