module.exports = function (express) {
    const router = express.Router(),
        fs = require('fs-extra'),
        path = require('path');

    router.get('*', function (req, res, next) {
        if (req.path.includes('/img/avatars/'))
            return res.download(path.join(global.config.paths.public, 'img', 'avatars', 'noavatar.png'));

        let noAuth = ['error400', 'error500', 'login'],
            urlParts = req.path.split('/').slice(1),
            controller = (urlParts[urlParts.length - 1] || ''),
            pathView = undefined;

        pathView = (req.user ? [req.user.interface] : []);

        if (controller === '' || ((controller === 'login') && req.isAuthenticated()))
            controller = 'index';

        function getController () {
            urlParts.forEach(function (item, index) {
                if (item !== '')
                    pathView.push(item);
            });

            if (!pathView.length)
                pathView.push(controller);

            if (!fs.existsSync(path.normalize(path.join(global.config.paths.views, pathView.join('/'))) + '.pug'))
                pathView.push('index');

            return path.normalize(pathView.join('/'));
        }

        if (noAuth.includes(controller))
            controller = getController();
        else {
            if (!req.isAuthenticated())
                return res.redirect('/auth/login');

            controller = getController();
        }

            return render_params(req, function (params) {
                return res.render(path.join(global.config.paths.views, controller + '.pug'), params);
            });
    });

    const render_params = function(req, cb) {
        return cb({
            params: Object.assign({}, req.query, req.params),
            user: req.user
        });
    };

    router.all('*', function (req, res, next) {
        return next(global.utils.httpError(400));
    });

    return router;
};