$().ready(function () {
    $('form input').keydown(function (e) {
        if (e.keyCode === 13) {
            e.preventDefault();
            return false;
        }
    });

    $('input#username')
        .keyup(function (e) {
            e.preventDefault();

            if (e.keyCode === 13)
                $('input#password').focus();
        })
        .focus();

    $('input#password').keyup(function (e) {
        e.preventDefault();

        if (e.keyCode === 13)
            $('button#submit').click();
    });

    $('button#submit').click(function (e) {
        e.preventDefault();

        let form = $('form#login');
        if (form[0].checkValidity() === false) {
            e.stopPropagation();

            $('.help-block').addClass('hidden');
            return form.addClass('was-validated');
        }

        form.addClass('was-validated');

        $('button#submit').fadeOut(function () {
            processAjax({
                type: 'POST',
                url: '/auth/login',
                dataType: 'json',
                data: form.serializeArray(),
            }, function (result) {
                if (!result){
                    form.removeClass('was-validated');
                    return $('button#submit').fadeIn();
                }

                $('#info-row').removeClass('hidden');
                $('#info').html(result.message);

                setTimeout(function () {
                    window.location = '/';
                }, 2000);
            });
        });
    });
});
