exports.get = function (req, res, next) {
    const path = require('path'),
        send = require('send'),
        readChunk = require('read-chunk'),
        fileType = require('file-type');
    let paramPath = path.join(global.config.paths.drive, req.query['path'] || '');

    return send(req, paramPath)
        .on('headers', function (res) {
            res.setHeader('Content-disposition', 'attachment; filename="' + encodeURI(path.basename(paramPath)) + '"');
            res.setHeader('Content-type', fileType(readChunk.sync(paramPath, 0, fileType.minimumBytes)).mime);
        })
        .pipe(res);
};